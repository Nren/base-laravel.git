<?php

namespace Peer\Base;

use Illuminate\Http\Resources\Json\ResourceCollection as BaseResourceCollection;
use Illuminate\Pagination\AbstractPaginator;

class ResourceCollection extends BaseResourceCollection{

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return method_exists($this,'setCollectionItem')?$this->setCollection($request):$this->collection->map->toArray($request)->all();
    }

    public function setCollection($request){
        $data=array();
        foreach($this->collection as $v){
            $data[]=$this->setCollectionItem($v);
        }
        return $data;
    }

    public function toResponse($request){
        return $this->resource instanceof AbstractPaginator
                    ? (new PaginatedResourceResponse($this))->toResponse($request)
                    : (new ResourceResponse($this))->toResponse($request);
    }
}